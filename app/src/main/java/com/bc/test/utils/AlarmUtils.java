package com.bc.test.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.bc.test.receivers.AlarmReceiver;
import com.bc.test.services.SyncTaskService;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * Created by binarymac1 on 18/08/2016.
 */

public class AlarmUtils {

    private static final long TASK_SYNC_INTERVAL_PER_DAY_IN_MILLI = TimeUnit.HOURS.toMillis(12);

    public static void setSyncWatchdogDailyAlarm(Context context){
        if(!checkAlarmCreated(context, SyncTaskService.SYNC_TASK_ID)){
            Calendar calendar = Calendar.getInstance();
            AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
            am.setRepeating(AlarmManager.RTC_WAKEUP,calendar.getTime().getTime(),TASK_SYNC_INTERVAL_PER_DAY_IN_MILLI,getPendingIntent(context,SyncTaskService.SYNC_TASK_ID));
        }
        else {
            Log.d(AlarmUtils.class.getName(),"alarm for task_sync has been created. we skip.");
        }
    }


    private static PendingIntent getPendingIntent(Context context,int taskId) {
        Bundle bundle = new Bundle();
        bundle.putInt(SyncTaskService.EXTRA_TASK_ID,taskId);
        Intent intent =  new Intent(context, AlarmReceiver.class);
        intent.putExtras(bundle);
        return PendingIntent.getBroadcast(context, taskId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    private static boolean checkAlarmCreated(Context context, int taskId){
        Intent intent =  new Intent(context, AlarmReceiver.class).putExtra(SyncTaskService.EXTRA_TASK_ID,taskId);
        final PendingIntent pendingIntent =
                PendingIntent.getBroadcast(context, taskId, intent,
                        PendingIntent.FLAG_NO_CREATE);
        return pendingIntent!=null;
    }

}
