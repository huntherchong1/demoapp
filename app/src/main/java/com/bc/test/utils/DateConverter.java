package com.bc.test.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by binarymac1 on 20/10/2016.
 */

public class DateConverter {
    public static final String parseDate(Date date, String format) throws ParseException {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }
}
