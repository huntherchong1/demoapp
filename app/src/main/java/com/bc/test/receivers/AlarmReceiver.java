package com.bc.test.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.bc.test.services.SyncTaskService;

public class AlarmReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceSyncIntent = new Intent(context, SyncTaskService.class);
        serviceSyncIntent.putExtras(intent.getExtras());
        context.startService(serviceSyncIntent);
    }
}
