package com.bc.test.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.bc.test.services.SyncTaskService;

/**
 * Created by binarymac1 on 16/08/2016.
 */

public class NetworkChangedReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            boolean isOnline = isOnline(context);
            if(isOnline){
                Intent serviceIntent = new Intent(context, SyncTaskService.class);
                Bundle bundle = new Bundle();
                bundle.putInt(SyncTaskService.EXTRA_TASK_ID,SyncTaskService.SYNC_TASK_ID);
                serviceIntent.putExtras(bundle);
                context.startService(serviceIntent);
            }
        }
        catch (Exception exp){

        }
    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());

    }
}
