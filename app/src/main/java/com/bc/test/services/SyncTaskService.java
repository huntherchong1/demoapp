package com.bc.test.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.bc.test.R;
import com.bc.test.activity.feed.MainActivity;
import com.bc.test.ba.BAManager;
import com.bc.test.ba.feed.FeedBa;
import com.bc.test.model.Feed;
import com.bc.test.service.ServiceManager;

import java.util.Calendar;
import java.util.List;


/**
 * Created by binarymac1 on 16/08/2016.
 */

public class SyncTaskService extends Service{

    public static final String EXTRA_TASK_ID = "EXTRA_TASK_ID";
    public static final int SYNC_TASK_ID = 10001;
    private boolean mRunning;
    private static int mTaskId;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        mRunning = false;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if(mRunning || intent==null){
            return super.onStartCommand(intent, flags, startId);
        }

        mTaskId = intent.getIntExtra(EXTRA_TASK_ID,0);
        if(mTaskId>0){
            runTask();
        }
        else {
            stopSelf();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void runTask(){
        ServiceManager.init(getApplicationContext());
        BAManager.getInstance().getFeedBa().retrieveFeed(new FeedBa.FeedListener() {
            @Override
            public void onCachedData(List<Feed> mLists) {

            }

            @Override
            public void onNewDataAvailable(List<Feed> mLists) {
                buildNewDataAvailableNotification();
                stopSelf();
            }

            @Override
            public void onError(Exception exp) {
                stopSelf();
            }
        });
    }

    private void buildNewDataAvailableNotification(){
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.place_holder)
                        .setContentTitle("New Content Available")
                        .setContentText("New Content Available");
        Intent resultIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        mTaskId,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(mTaskId, mBuilder.build());
    }

}
