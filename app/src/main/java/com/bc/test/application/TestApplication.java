package com.bc.test.application;

import android.app.Application;

import com.bc.test.service.ServiceManager;
import com.bc.test.utils.AlarmUtils;

/**
 * Created by binarymac1 on 20/10/2016.
 */

public class TestApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        ServiceManager.init(getApplicationContext());

        AlarmUtils.setSyncWatchdogDailyAlarm(getApplicationContext());
    }
}
