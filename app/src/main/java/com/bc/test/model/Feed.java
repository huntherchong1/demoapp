package com.bc.test.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.bc.test.utils.DateConverter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by binarymac1 on 14/10/2016.
 */

public class Feed implements Parcelable {

    private String imageUrl;
    private String title;
    private String date;
    private String time;
    private String description;
    private String detailDescription;
    private String link;

    public Feed(String imageUrl, String title, String date, String time, String description, String detailDescription, String link) {
        this.imageUrl = imageUrl;
        this.title = title;
        this.date = date;
        this.time = time;
        this.description = description;
        this.detailDescription = detailDescription;
        this.link = link;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getDescription() {
        return description;
    }

    public String getDetailDescription() {
        return detailDescription;
    }

    public String getLink() {
        return link;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imageUrl);
        dest.writeString(this.title);
        dest.writeString(this.date);
        dest.writeString(this.time);
        dest.writeString(this.description);
        dest.writeString(this.detailDescription);
        dest.writeString(this.link);
    }

    protected Feed(Parcel in) {
        this.imageUrl = in.readString();
        this.title = in.readString();
        this.date = in.readString();
        this.time = in.readString();
        this.description = in.readString();
        this.detailDescription = in.readString();
        this.link = in.readString();
    }

    public static final Parcelable.Creator<Feed> CREATOR = new Parcelable.Creator<Feed>() {
        @Override
        public Feed createFromParcel(Parcel source) {
            return new Feed(source);
        }

        @Override
        public Feed[] newArray(int size) {
            return new Feed[size];
        }
    };

    public synchronized static final List<Feed> parseFeeds(JSONArray data) throws JSONException, ParseException {
        if(data==null || data.length()==0){
            return null;
        }

        List<Feed> feeds = new ArrayList<Feed>();
        for(int i=0;i<data.length();i++){
            feeds.add(parseFeed(data.getJSONObject(i),i==0));
        }

        return feeds;
    }

    public synchronized static final Feed parseFeed(JSONObject feedData, boolean landscapeImage) throws JSONException, ParseException {
        JSONArray mediaGroup = feedData.has("mediaGroups")?feedData.getJSONArray("mediaGroups"):null;
        Date date = new Date(feedData.getString("publishedDate"));

        JSONArray contents = null;
        JSONObject targetImageContent = null;

        if(mediaGroup!=null && mediaGroup.length() > 0){
            contents = mediaGroup.getJSONObject(0).getJSONArray("contents");

            if(landscapeImage){
                for(int i=0;i<mediaGroup.length();i++){
                    int width = contents.getJSONObject(i).getInt("width");
                    int height = contents.getJSONObject(i).getInt("height");
                    //Look for landscape image
                    if(width > height){
                        targetImageContent = contents.getJSONObject(i);
                        break;
                    }
                }
            }
            else{
                int targetIndex = 0;
                int smallestSize = 0;
                for(int i=0;i<mediaGroup.length();i++){
                    int width = contents.getJSONObject(i).getInt("width");
                    int height = contents.getJSONObject(i).getInt("height");
                    int total = width * height;
                    if(i==0){
                        smallestSize = total;
                    }
                    else if(total<smallestSize){
                        targetIndex=i;
                        smallestSize = total;
                    }
                }
                targetImageContent = contents.getJSONObject(targetIndex);
            }
        }

        return new Feed(targetImageContent!=null?targetImageContent.getString("url"):"",feedData.getString("title"), DateConverter.parseDate(date,"MMM d,yyyy"),DateConverter.parseDate(date,"hh:mm a"),feedData.getString("contentSnippet"),feedData.getString("content"),feedData.getString("link"));
    }
}
