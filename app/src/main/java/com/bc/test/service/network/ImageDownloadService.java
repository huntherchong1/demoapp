package com.bc.test.service.network;

import android.content.Context;
import android.graphics.Bitmap;

/**
 * Created by binarymac1 on 20/10/2016.
 */

public interface ImageDownloadService {

    /**
     * Initialization
     * @param context
     */
    public void init(Context context);

    /**
     * Download image from the url
     * @param url
     * @return
     */
    public Bitmap getImage(String url);

    /**
     * Download image from the url with expected size
     * @param url
     * @param expectedWidth
     * @param expectedHeight
     * @return
     */
    public Bitmap getImage(String url, int expectedWidth, int expectedHeight);
}
