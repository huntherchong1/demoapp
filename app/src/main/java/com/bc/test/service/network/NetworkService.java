package com.bc.test.service.network;

/**
 * Created by binarymac1 on 20/10/2016.
 */

public interface NetworkService {

    /**
     * Send get request from the url
     * @param url
     * @param requestListener
     */
    public void getRequest(final String url, final OnGetRequestListener requestListener);

    public interface OnGetRequestListener{
        public void onError(Exception exp);
        public void onSuccess(String result);
    }
}
