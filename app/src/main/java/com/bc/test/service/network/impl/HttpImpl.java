package com.bc.test.service.network.impl;

import android.os.AsyncTask;
import android.util.Log;

import com.bc.test.service.network.NetworkService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by binarymac1 on 20/10/2016.
 */

public class HttpImpl implements NetworkService{

    @Override
    public void getRequest(final String remoteUrl,final OnGetRequestListener requestListener) {
        AsyncTask requestTask = new AsyncTask() {

            private Exception exception;
            private String result;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Object doInBackground(Object[] objects) {
                try {
                    result = request(remoteUrl);
                } catch (IOException e) {
                    exception = e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                if(requestListener!=null){
                    if(exception!=null){
                        requestListener.onError(exception);
                    }
                    else{
                        requestListener.onSuccess(result);
                    }
                }
                super.onPostExecute(o);
            }
        };
        requestTask.execute();
    }

    private String request(String remoteUrl) throws IOException {
        StringBuffer contentAsString = new StringBuffer();
        HttpURLConnection conn = null;
        try {
            URL url = new URL(remoteUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");

            int responseCode = conn.getResponseCode();
            Log.d("test","\nSending 'GET' request to URL : " + url);
            Log.d("test","Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                contentAsString.append(inputLine);
            }
            in.close();
        }
        finally {
            if(conn!=null){
                conn.disconnect();
            }
        }
        return contentAsString.toString();
    }
}
