package com.bc.test.service.cache.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.bc.test.service.cache.ImageCacheService;

/**
 * Created by binarymac1 on 20/10/2016.
 */

public class LRUImageCache implements ImageCacheService{

    private LruCache<String,Bitmap> mCache;

    //In the future can put it as configurable component
    private int oneMB = 1024;
    //In the future can put it as configurable component
    private int maxCacheFromMaxStorage = 8;

    @Override
    public void init(Context context) {

        //MAX MEMORY
        final int maxMemory = (int) ((Runtime.getRuntime()).maxMemory() / oneMB);

        //MAX CACHE SIZE
        final int cacheSize = maxMemory / maxCacheFromMaxStorage;

        mCache = new LruCache<String, Bitmap>(cacheSize)
        {
            protected int sizeOf(String key, Bitmap value)
            {
                //return bitmap size
                int bitmapByteCount = value.getRowBytes() * value.getHeight();
                return bitmapByteCount / oneMB;
            }
        };
    }

    @Override
    public void addImage(String url, Bitmap bitmap) {
        if(mCache != null && url != null && mCache.get(url) == null )
        {
            mCache.put(url, bitmap);
        }
    }

    @Override
    public Bitmap getImage(String url) {
        if(mCache != null)
        {
            return mCache.get(url);
        }
        return null;
    }

    @Override
    public void removeImage(String url) {
        if(mCache != null){
            mCache.remove(url);
        }
    }

    @Override
    public boolean isInCache(String url) {
        if(mCache != null){
            return mCache.get(url)!=null;
        }
        return false;
    }

    @Override
    public void removeAll() {
        if(mCache != null){
            mCache.evictAll();
        }
    }
}
