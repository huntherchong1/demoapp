package com.bc.test.service.storage;

import android.content.Context;

/**
 * Created by binarymac1 on 20/10/2016.
 */

public interface StorageService {

    /**
     * Init method
     * @param context
     */
    public void init(Context context);

    /**
     * Store Key Value to persistence storage
     * @param key
     * @param value
     */
    public void storeRecord(String key, String value);

    /**
     * Retrieve record from persistence storage
     * @param key
     * @return
     */
    public String retrieveRecord(String key);

    /**
     * Clear all records from persistence storage
     */
    public void clearRecord();
}
