package com.bc.test.service.network.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Display;

import com.bc.test.service.ServiceManager;
import com.bc.test.service.network.ImageDownloadService;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by binarymac1 on 20/10/2016.
 */

public class CachableImageDownloadImpl implements ImageDownloadService{

    private int inSampleSize = 0;
    //In the future can put it as configurable component
    private int mExpectedWidth = 300;
    //In the future can put it as configurable component
    private int mExpectedHeight = 300;

    @Override
    public void init(Context context) {

    }

    public Bitmap getImage(String imageUrl)
    {
        //Cached
        Bitmap image = ServiceManager.requestCacheService().getImage(imageUrl);
        if(image!=null){
            return image;
        }

        image = downloadImage(imageUrl);
        if(image!=null)
            ServiceManager.requestCacheService().addImage(imageUrl,image);

        return image;
    }

    @Override
    public Bitmap getImage(String url, int expectedWidth, int expectedHeight) {
        mExpectedWidth = expectedWidth;
        mExpectedHeight = expectedHeight;
        return getImage(url);
    }

    private Bitmap downloadImage(String imageUrl){

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inSampleSize = inSampleSize;

        try
        {
            URL url = new URL(imageUrl);

            HttpURLConnection connection = (HttpURLConnection)url.openConnection();

            InputStream stream = connection.getInputStream();

            BitmapFactory.decodeStream(stream, null, options);

            int imageWidth = options.outWidth;

            int imageHeight = options.outHeight;

            if(imageWidth > mExpectedWidth || imageHeight > mExpectedHeight)
            {
                inSampleSize = inSampleSize + 2;

                return getImage(imageUrl);
            }
            else
            {
                options.inJustDecodeBounds = false;

                connection = (HttpURLConnection)url.openConnection();

                stream = connection.getInputStream();

                Bitmap image = BitmapFactory.decodeStream(stream, null, options);

                return image;
            }
        }

        catch(Exception e)
        {
            return null;
        }
    }
}
