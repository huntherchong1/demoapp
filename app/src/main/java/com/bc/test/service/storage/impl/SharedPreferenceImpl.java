package com.bc.test.service.storage.impl;

import android.content.Context;
import android.content.SharedPreferences;

import com.bc.test.service.storage.StorageService;

/**
 * Created by binarymac1 on 20/10/2016.
 */

public class SharedPreferenceImpl implements StorageService{

    private static final String PREV_NAME="XXXYYYZZZ";
    private Context mContext;

    @Override
    public void init(Context context) {
        mContext = context;
    }

    @Override
    public void storeRecord(String key, String value) {
        SharedPreferences sp = mContext.getSharedPreferences(PREV_NAME,Context.MODE_PRIVATE);
        sp.edit().putString(key,value).commit();
    }

    @Override
    public String retrieveRecord(String key) {
        SharedPreferences sp = mContext.getSharedPreferences(PREV_NAME,Context.MODE_PRIVATE);
        return sp.getString(key,"");
    }

    @Override
    public void clearRecord() {
        mContext.getSharedPreferences(PREV_NAME,Context.MODE_PRIVATE).edit().clear().commit();
    }
}
