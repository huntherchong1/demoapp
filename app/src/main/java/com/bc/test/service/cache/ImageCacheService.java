package com.bc.test.service.cache;

import android.content.Context;
import android.graphics.Bitmap;

/**
 * Created by binarymac1 on 20/10/2016.
 */

public interface ImageCacheService {

    /**
     * Initialize the service
     * @param context
     */
    public void init(Context context);

    /**
     * Add image to the cache
     * @param url
     * @param bitmap
     */
    public void addImage(String url, Bitmap bitmap);

    /**
     * Get image from the cache
     * @param url
     * @return
     */
    public Bitmap getImage(String url);

    /**
     * Remove image from the cache
     * @param url
     */
    public void removeImage(String url);

    /**
     * Check image is the the cache
     * @param url
     * @return
     */
    public boolean isInCache(String url);

    /**
     * Clear items in the cache
     */
    public void removeAll();
}
