package com.bc.test.service;

import android.content.Context;

import com.bc.test.service.cache.ImageCacheService;
import com.bc.test.service.cache.impl.LRUImageCache;
import com.bc.test.service.network.ImageDownloadService;
import com.bc.test.service.network.NetworkService;
import com.bc.test.service.network.impl.CachableImageDownloadImpl;
import com.bc.test.service.network.impl.HttpImpl;
import com.bc.test.service.storage.StorageService;
import com.bc.test.service.storage.impl.SharedPreferenceImpl;


/**
 * Service Manager which will abstract services needed by the application
 */
public class ServiceManager {

    private static NetworkService mNetworkService;
    private static StorageService mStorageService;
    private static ImageCacheService mImageCacheService;
    private static ImageDownloadService mImageDownloadService;

    /**
     * Need to call during the application starts up
     * @param context
     */
    public static void init(Context context){
        if(mNetworkService==null){
            mNetworkService = new HttpImpl();
        }
        if(mStorageService==null){
            mStorageService = new SharedPreferenceImpl();
            mStorageService.init(context);
        }
        if(mImageCacheService==null){
            mImageCacheService = new LRUImageCache();
            mImageCacheService.init(context);
        }
        if(mImageDownloadService==null){
            mImageDownloadService = new CachableImageDownloadImpl();
        }
    }

    /**
     * Request network service which allows you to send network request to the internet
     * @return NetworkService
     */
    public static NetworkService requestNetworkService(){
        return mNetworkService;
    }

    /**
     * Request storage service which allows you to store data in the persistent storage
     * @return StorageService
     */
    public static StorageService requestStorageService(){
        return mStorageService;
    }

    /**
     * Request image cache service which allows you to store image file in Cache
     * Note: Currently LRUCache is the only cache system inplace.
     *       to enhance performance we can implement both DISK LRU + LRU in the future
     * @return
     */
    public static ImageCacheService requestCacheService(){
        return mImageCacheService;
    }

    /**
     *  Download image from the internet. Currently it's implement along side with Cache service
     * @return ImageDownloadService
     */
    public static ImageDownloadService requestImageDownloadService(){
        return mImageDownloadService;
    }

}
