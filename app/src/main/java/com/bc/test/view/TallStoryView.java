package com.bc.test.view;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bc.test.R;
import com.bc.test.ba.BAManager;
import com.bc.test.model.Feed;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by binarymac1 on 14/10/2016.
 */

public class TallStoryView extends CardView{

    @BindView(R.id.view_long_story_title_img)
    ImageView mImageView;

    @BindView(R.id.view_long_story_title_txt)
    TextView mTextView;

    @BindView(R.id.view_long_story_date_txt)
    TextView mDateTextView;

    @BindView(R.id.view_long_story_time_txt)
    TextView mTimeTextView;


    public TallStoryView(Context context) {
        super(context);
        initView();
    }

    public TallStoryView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public TallStoryView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView(){
        inflate(getContext(), R.layout.view_long_story,this);
        ButterKnife.bind(this);

        int cardEle = (int) getCardElevation();

        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(cardEle,cardEle,cardEle,cardEle);
        setLayoutParams(lp);

        setPreventCornerOverlap(false);
        setUseCompatPadding(false);
        setCardBackgroundColor(Color.parseColor("#FFFFFF"));
        setCardElevation(cardEle);
    }

    public void setData(Feed data) {
        mTextView.setText(data.getTitle());
        mDateTextView.setText(data.getDate());
        mTimeTextView.setText(data.getTime());

        BAManager.getInstance().getFeedBa().bindImageView(data.getImageUrl(),new WeakReference<ImageView>(mImageView),R.drawable.place_holder);
    }
}
