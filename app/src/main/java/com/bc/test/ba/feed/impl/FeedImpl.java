package com.bc.test.ba.feed.impl;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.os.AsyncTaskCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import com.bc.test.ba.feed.FeedBa;
import com.bc.test.model.Feed;
import com.bc.test.service.ServiceManager;
import com.bc.test.service.network.NetworkService;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by binarymac1 on 20/10/2016.
 */

public class FeedImpl implements FeedBa{

    private static final String FEED_KEY = "FEED_KEY";
    private String url = "http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=25&q=http://www.abc.net.au/news/feed/51120/rss.xml";
    private int mQueueSize = 5;
    private List<DownloadImageTask> mTaskQueue = new ArrayList<>();

    @Override
    public void retrieveFeed(final FeedListener listener) {

        String cachedData = ServiceManager.requestStorageService().retrieveRecord(FEED_KEY);
        try {
            if(listener!=null){
                listener.onCachedData(parseFeedData(cachedData));
            }
        } catch (JSONException e) {
            listener.onError(e);
        } catch (ParseException e) {
            listener.onError(e);
        }

        ServiceManager.requestNetworkService().getRequest(url, new NetworkService.OnGetRequestListener() {
            @Override
            public void onError(Exception exp) {
                if(listener!=null){
                    listener.onError(exp);
                }
            }

            @Override
            public void onSuccess(String result) {
                ServiceManager.requestStorageService().storeRecord(FEED_KEY,result);
                if(listener!=null){
                    try {
                        listener.onNewDataAvailable(parseFeedData(result));
                    } catch (JSONException e) {
                        listener.onError(e);
                    } catch (ParseException e) {
                        listener.onError(e);
                    }
                }
            }
        });

    }

    @Override
    public void clearCache() {
        ServiceManager.requestStorageService().clearRecord();
    }

    @Override
    public void bindImageView(String url, WeakReference weakReference, int holderDrawable) {
        ImageView iv = (ImageView) weakReference.get();
        DownloadImageTask downloadImageTask = new DownloadImageTask(url,weakReference,iv.getHeight(),iv.getWidth(),holderDrawable);
        if(mTaskQueue.size()>=mQueueSize){
            mTaskQueue.get(0).cancel(false);
            mTaskQueue.remove(0);
        }
        mTaskQueue.add(downloadImageTask);
        AsyncTaskCompat.executeParallel(downloadImageTask);
    }

    private class DownloadImageTask extends AsyncTask<Void, Void, Bitmap> {

        WeakReference mWeakReference;
        String mUrl;
        int mHolderDrawable;
        int mExpectedHeight;
        int mExpectedWidth;


        DownloadImageTask(String url, WeakReference ivReference, int expectedHeight, int expectedWidth, int holderDrawable){
            mUrl = url;
            mWeakReference = ivReference;
            mHolderDrawable = holderDrawable;
            mExpectedHeight = expectedHeight;
            mExpectedWidth = expectedWidth;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(mWeakReference!=null && !ServiceManager.requestCacheService().isInCache(url)){
                if(mWeakReference!=null){
                    ImageView imageView = (ImageView) mWeakReference.get();
                    imageView.setImageResource(mHolderDrawable);
                }
            }
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            if(isCancelled()){
                return null;
            }
            if(mExpectedHeight > 0 && mExpectedWidth > 0){
                return ServiceManager.requestImageDownloadService().getImage(mUrl,mExpectedWidth,mExpectedHeight);
            }
            else {
                return ServiceManager.requestImageDownloadService().getImage(mUrl);
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            if(isCancelled() || bitmap == null){
                return;
            }

            if(mWeakReference!=null && bitmap!=null){
                ImageView imageView = (ImageView) mWeakReference.get();
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    private List<Feed> parseFeedData(String data) throws JSONException, ParseException {

        if(TextUtils.isEmpty(data)){
            return null;
        }

        JSONObject feedData = new JSONObject(data);
        return Feed.parseFeeds(feedData.getJSONObject("responseData").getJSONObject("feed").getJSONArray("entries"));
    }
}
