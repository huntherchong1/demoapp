package com.bc.test.ba;

import com.bc.test.ba.feed.FeedBa;
import com.bc.test.ba.feed.impl.FeedImpl;

/**
 * Created by binarymac1 on 20/10/2016.
 */

public class BAManager {
    private static BAManager mInstance;
    private FeedBa mFeedBa;

    /**
     * Get a singleton of the Business Logic manager
     * @return
     */
    public static BAManager getInstance() {
        if(mInstance==null){
            mInstance = new BAManager();
        }
        return mInstance;
    }

    /**
     * BAManger constructor - will init all the business logic component
     */
    public BAManager(){
        mFeedBa = new FeedImpl();
    }

    /**
     * Business Logic for feed
     * @return
     */
    public FeedBa getFeedBa(){
        return mFeedBa;
    }

}
