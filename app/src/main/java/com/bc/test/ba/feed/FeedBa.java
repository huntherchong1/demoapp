package com.bc.test.ba.feed;

import com.bc.test.model.Feed;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Summarize all the business logic needed from the UI
 */
public interface FeedBa {

    /**
     * Retrieve feeds
     * @param listener
     */
    public void retrieveFeed(FeedListener listener);

    /**
     * Clear the local copy of the cache
     */
    public void clearCache();

    /**
     * Bind image view with the url
     * @param url
     * @param weakReference
     * @param holderDrawable
     */
    public void bindImageView(String url, WeakReference weakReference, int holderDrawable);

    /**
     * Interface with allow user to retrieve cache data and new data
     */
    public interface FeedListener{
        public void onCachedData(List<Feed> mLists);
        public void onNewDataAvailable(List<Feed> mLists);
        public void onError(Exception exp);
    }

}
