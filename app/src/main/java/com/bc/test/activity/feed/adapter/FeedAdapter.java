package com.bc.test.activity.feed.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.bc.test.activity.feed.FeedDetailActivity;
import com.bc.test.activity.feed.FeedDetailActivity2;
import com.bc.test.model.Feed;
import com.bc.test.view.ShortStoryView;
import com.bc.test.view.TallStoryView;

import java.util.List;

/**
 * Created by binarymac1 on 14/10/2016.
 */

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> implements View.OnClickListener{

    private static final int TYPE_TALL = 0;
    private static final int TYPE_SHORT = 1;

    private Context mContext;
    private List<Feed> mData;

    public FeedAdapter(Context context) {
        mContext = context;
    }

    public void updateData(List<Feed> data){
        mData = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(mData==null){
            return 0;
        }
        return mData.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        if(getItemViewType(i) == TYPE_SHORT){
            ShortStoryView view = (ShortStoryView) viewHolder.itemView;
            view.setData(mData.get(i));
        }
        else if(getItemViewType(i) == TYPE_TALL){
            TallStoryView view = (TallStoryView) viewHolder.itemView;
            view.setData(mData.get(i));
        }
        viewHolder.itemView.setTag(i);
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return TYPE_TALL;
        }
        return TYPE_SHORT;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = null;
        if(getItemViewType(i) == TYPE_SHORT){
            itemView = new ShortStoryView(mContext);
        }
        else if(getItemViewType(i) == TYPE_TALL){
            itemView = new TallStoryView(mContext);
        }
        itemView.setOnClickListener(this);
        return new ViewHolder(itemView);
    }

    @Override
    public void onClick(View view) {
        int index = (int) view.getTag();
        Intent intent = new Intent(mContext, FeedDetailActivity2.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle();
        bundle.putParcelable(FeedDetailActivity.FEED_DETAIL_DATA,mData.get(index));
        intent.putExtras(bundle);
        mContext.startActivity(intent);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}