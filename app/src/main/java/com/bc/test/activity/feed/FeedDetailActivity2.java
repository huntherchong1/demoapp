package com.bc.test.activity.feed;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.bc.test.R;
import com.bc.test.activity.BaseActivity;
import com.bc.test.model.Feed;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedDetailActivity2 extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{

    public static final String FEED_DETAIL_DATA = "FEED_DETAIL_DATA";

    @BindView(R.id.activity_feed_details_webview)
    WebView mWebView;

    @BindView(R.id.activity_main_list_str)
    SwipeRefreshLayout mRefreshLayout;

    private ShareActionProvider mShareActionProvider;
    private Feed mFeedData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Load Data
        Intent intent = getIntent();
        if(intent == null || intent.getParcelableExtra(FEED_DETAIL_DATA) == null){
            Toast.makeText(getBaseContext(),"Activity Required Data Not Exist.",Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        mFeedData = intent.getParcelableExtra(FEED_DETAIL_DATA);

        setContentView(R.layout.activity_feed_details_2);
        ButterKnife.bind(this);

        showActionBar();
        showActionBarBack();

        setupView();
        bindData();
    }

    private void bindData() {
        mRefreshLayout.setRefreshing(true);
        mWebView.loadUrl(mFeedData.getLink());
    }


    private void setupShareIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TITLE, mFeedData.getTitle());
        sendIntent.putExtra(Intent.EXTRA_TEXT, mFeedData.getLink());
        sendIntent.setType("text/plain");
        setShareIntent(sendIntent);
    }

    private void setupView() {
        mWebView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                showError(getString(R.string.error_retrieve_link));
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                showError(getString(R.string.error_retrieve_link));
            }
        });
        mRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.feed_detail_menu, menu);

        MenuItem item = menu.findItem(R.id.menu_item_share);

        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        setupShareIntent();

        return true;
    }

    // Call to update the share intent
    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }

    @Override
    public void onRefresh() {
        bindData();
    }
}
