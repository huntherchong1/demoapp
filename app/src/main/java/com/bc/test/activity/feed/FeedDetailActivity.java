package com.bc.test.activity.feed;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.ShareActionProvider;
import android.text.Html;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bc.test.R;
import com.bc.test.activity.BaseActivity;
import com.bc.test.model.Feed;
import com.bc.test.view.TallStoryView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedDetailActivity extends BaseActivity implements NestedScrollView.OnScrollChangeListener{

    private static final int OFFSET = 400;
    public static final String FEED_DETAIL_DATA = "FEED_DETAIL_DATA";

    @BindView(R.id.activity_feed_details_scrollview)
    NestedScrollView mScrollView;

    @BindView(R.id.activity_feed_details_beam_me_up)
    View mQuickReturnView;

    @BindView(R.id.activity_feed_details_top_storyview)
    TallStoryView mStoryView;

    @BindView(R.id.activity_feed_details_details)
    TextView mStoryText;




    private ShareActionProvider mShareActionProvider;
    private Feed mFeedData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Load Data
        Intent intent = getIntent();
        if(intent == null || intent.getParcelableExtra(FEED_DETAIL_DATA) == null){
            Toast.makeText(getBaseContext(),"Activity Required Data Not Exist.",Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        mFeedData = intent.getParcelableExtra(FEED_DETAIL_DATA);

        setContentView(R.layout.activity_feed_details);
        ButterKnife.bind(this);

        showActionBar();
        showActionBarBack();

        setupView();
        bindData();
    }

    private void bindData() {
        mStoryView.setData(mFeedData);

        if(!TextUtils.isEmpty(mFeedData.getDetailDescription())){
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                mStoryText.setText(Html.fromHtml(mFeedData.getDetailDescription(),Html.FROM_HTML_MODE_LEGACY));
            } else {
                mStoryText.setText(Html.fromHtml(mFeedData.getDetailDescription()));
            }
        }
    }


    private void setupShareIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TITLE, mFeedData.getTitle());
        sendIntent.putExtra(Intent.EXTRA_TEXT, mFeedData.getDescription());
        sendIntent.setType("text/plain");
        setShareIntent(sendIntent);
    }

    private void showQuickReturnBar(){
        if(mQuickReturnView.getY()<=0){
            int value = mQuickReturnView.getHeight();
            mQuickReturnView.animate().y(value).alpha(1);
        }
    }

    private void hideQuickReturnBar(){
        if(mQuickReturnView.getY()>=0){
            int value = -mQuickReturnView.getHeight();
            mQuickReturnView.animate().y(value).alpha(0);
        }
    }

    private void setupView() {
        mScrollView.setOnScrollChangeListener(this);
        mQuickReturnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideQuickReturnBar();
                mScrollView.scrollTo(0,0);
            }
        });
        hideQuickReturnBar();
    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
       if(scrollY > OFFSET){
            showQuickReturnBar();
        }
        else{
            hideQuickReturnBar();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.feed_detail_menu, menu);

        MenuItem item = menu.findItem(R.id.menu_item_share);

        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        setupShareIntent();

        return true;
    }

    // Call to update the share intent
    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }
}
