package com.bc.test.activity;

import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.bc.test.R;

/**
 * Created by binarymac1 on 14/10/2016.
 */

public class BaseActivity extends AppCompatActivity{
    protected void showActionBar(){

        Toolbar actionToolBar = (Toolbar) findViewById(R.id.actionBar);
        if(actionToolBar!=null){
            setSupportActionBar(actionToolBar);
            getSupportActionBar().setTitle(null);
            getSupportActionBar().setDisplayShowCustomEnabled(false);
            getSupportActionBar().setDisplayUseLogoEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    protected void showActionBarBack(){
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null)
            actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showError(String message){
        Snackbar.make(findViewById(android.R.id.content),message,Snackbar.LENGTH_SHORT).show();
    }
}
