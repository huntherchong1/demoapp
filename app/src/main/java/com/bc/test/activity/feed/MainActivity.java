package com.bc.test.activity.feed;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;

import com.bc.test.R;
import com.bc.test.activity.BaseActivity;
import com.bc.test.activity.feed.adapter.FeedAdapter;
import com.bc.test.activity.feed.adapter.VerticalSpaceItemDecoration;
import com.bc.test.ba.BAManager;
import com.bc.test.ba.feed.FeedBa;
import com.bc.test.model.Feed;
import com.bc.test.utils.UnitConverter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.activity_main_list)
    RecyclerView mFeedList;

    @BindView(R.id.activity_main_list_str)
    SwipeRefreshLayout mSwipeRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        showActionBar();
        setupView();
        loadData();
    }

    private void setupView() {
        mFeedList.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        mFeedList.addItemDecoration(new VerticalSpaceItemDecoration((int) UnitConverter.convertDpToPixel(8,getBaseContext())));
        mFeedList.setAdapter(new FeedAdapter(getBaseContext()));
        mSwipeRefresh.setOnRefreshListener(this);
    }

    private void loadData() {

        mSwipeRefresh.setRefreshing(true);
        BAManager.getInstance().getFeedBa().retrieveFeed(new FeedBa.FeedListener() {
            @Override
            public void onCachedData(List<Feed> mLists) {
                FeedAdapter adapter = (FeedAdapter) mFeedList.getAdapter();
                adapter.updateData(mLists);
            }

            @Override
            public void onNewDataAvailable(List<Feed> mLists) {
                mSwipeRefresh.setRefreshing(false);
                FeedAdapter adapter = (FeedAdapter) mFeedList.getAdapter();
                adapter.updateData(mLists);
            }

            @Override
            public void onError(Exception exp) {
                mSwipeRefresh.setRefreshing(false);
                //TODO More works need to be done to properly translate the exception
                showError(getString(R.string.error_retrieve_feed));
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }


    @Override
    public void onRefresh() {
        loadData();
    }
}
