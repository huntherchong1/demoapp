package com.bc.test.service;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.bc.test.service.network.NetworkService;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ServiceHttpImplTest {
    @Test
    public void testNormalHttpRequest() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        Context appContext = InstrumentationRegistry.getTargetContext();
        String url = "http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=25&q=http://www.abc.net.au/news/feed/51120/rss.xml";
        ServiceManager.init(appContext);
        ServiceManager.requestNetworkService().getRequest(url, new NetworkService.OnGetRequestListener() {
            @Override
            public void onError(Exception exp) {
                Assert.fail(exp.toString());
                latch.countDown();
            }

            @Override
            public void onSuccess(String result) {
                Assert.assertTrue(result != null);
                Assert.assertTrue(result.length()>0);
                latch.countDown();
            }
        });

        latch.await();
    }

    /**
     * Test offline mode
     * @throws Exception
     */
    public void testNoNetworkRequest() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        Context appContext = InstrumentationRegistry.getTargetContext();
        String url = "http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=25&q=http://www.abc.net.au/news/feed/51120/rss.xml";
        ServiceManager.init(appContext);
        ServiceManager.requestNetworkService().getRequest(url, new NetworkService.OnGetRequestListener() {
            @Override
            public void onError(Exception exp) {
                Assert.assertTrue(exp!=null);
                latch.countDown();
            }

            @Override
            public void onSuccess(String result) {
                Assert.fail("Should not be success");
                latch.countDown();
            }
        });

        latch.await();
    }
}
