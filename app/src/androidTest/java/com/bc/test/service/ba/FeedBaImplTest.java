package com.bc.test.service.ba;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.bc.test.ba.feed.FeedBa;
import com.bc.test.ba.feed.impl.FeedImpl;
import com.bc.test.model.Feed;
import com.bc.test.service.ServiceManager;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by binarymac1 on 20/10/2016.
 */

@RunWith(AndroidJUnit4.class)
public class FeedBaImplTest {
    @Test
    public void testFirstTimeFeedRetrieval() throws Exception {

        Context appContext = InstrumentationRegistry.getTargetContext();
        ServiceManager.init(appContext);

        final CountDownLatch latch = new CountDownLatch(2);
        FeedImpl feedImpl = new FeedImpl();
        feedImpl.clearCache();
        feedImpl.retrieveFeed(new FeedBa.FeedListener() {
            @Override
            public void onCachedData(List<Feed> mLists) {
                Assert.assertNull(mLists);
                latch.countDown();
            }

            @Override
            public void onNewDataAvailable(List<Feed> mLists) {
                Assert.assertNotNull(mLists);
                latch.countDown();
            }

            @Override
            public void onError(Exception exp) {
                Assert.fail();
                latch.countDown();
            }
        });
        latch.await();
    }


    @Test
    public void testCacheFeedRetrieval() throws Exception {

        Context appContext = InstrumentationRegistry.getTargetContext();
        ServiceManager.init(appContext);

        final CountDownLatch latch = new CountDownLatch(2);
        FeedImpl feedImpl = new FeedImpl();
        feedImpl.clearCache();
        feedImpl.retrieveFeed(new FeedBa.FeedListener() {
            @Override
            public void onCachedData(List<Feed> mLists) {
                Assert.assertNull(mLists);
                latch.countDown();
            }

            @Override
            public void onNewDataAvailable(List<Feed> mLists) {
                Assert.assertNotNull(mLists);
                latch.countDown();
            }

            @Override
            public void onError(Exception exp) {
                Assert.fail();
                latch.countDown();
            }
        });
        latch.await();
    }

}
