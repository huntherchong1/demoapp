package com.bc.test.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.test.InstrumentationRegistry;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by binarymac1 on 20/10/2016.
 */

public class CachableImageDownloadImplTest {
    @Test
    public void testDownloadImage() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        String url = "http://www.abc.net.au/news/image/7952128-16x9-2150x1210.jpg";
        ServiceManager.init(appContext);
        Bitmap test = ServiceManager.requestImageDownloadService().getImage(url);
        Assert.assertTrue(test!=null);
        Assert.assertTrue(test.getByteCount()>0);
    }
}
