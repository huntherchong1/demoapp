package com.bc.test.service;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class SharedPrefStorageImplTest {

    @Test
    public void testStoreAndRetrieveData() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        ServiceManager.init(appContext);
        ServiceManager.requestStorageService().storeRecord("k1","v1");
        Assert.assertEquals(ServiceManager.requestStorageService().retrieveRecord("k1"),"v1");
    }

    @Test
    public void testClearData() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        ServiceManager.init(appContext);
        ServiceManager.requestStorageService().storeRecord("k1","v1");
        ServiceManager.requestStorageService().clearRecord();
        Assert.assertEquals(ServiceManager.requestStorageService().retrieveRecord("k1"),null);
    }
}
